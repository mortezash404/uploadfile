﻿using System.IO;
using System.Linq;

namespace UploadWebApp.Models
{
    public class PhotoSetting
    {
        public int MaxLength { get; set; }
        public string[] AllowedTypes { get; set; }

        public string Address { get; set; }

        public bool IsSuportedFileType(string fileName)
        {
            return AllowedTypes.Any(s=> s == Path.GetExtension(fileName).ToLower());
        }
    }
}
