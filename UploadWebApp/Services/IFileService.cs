﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace UploadWebApp.Services
{
    public interface IFileService
    {
        Task<string> StorePhoto(string uploadsFolderPath, IFormFile file);
    }
}