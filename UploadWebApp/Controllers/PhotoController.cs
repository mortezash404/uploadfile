﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using UploadWebApp.Models;
using UploadWebApp.Services;

namespace UploadWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhotoController : ControllerBase
    {
        private readonly PhotoSetting _photoSetting;
        private readonly IFileService _service;

        public PhotoController(IOptions<PhotoSetting> photoSetting, IFileService service)
        {
            _service = service;
            _photoSetting = photoSetting.Value;
        }

        [HttpPost]
        public async Task<IActionResult> Post(IFormFile file)
        {

            if (file == null)
                return BadRequest("file null");

            if (file.Length == 0)
                return BadRequest("empty file");

            if (file.Length > _photoSetting.MaxLength)
                return BadRequest("exceed file size.");

            if (!_photoSetting.IsSuportedFileType(file.FileName))
                return BadRequest("invalid file type.");

            var uploadsFolderPath = Path.Combine(_photoSetting.Address, "uploads");

            var storePhoto = await _service.StorePhoto(uploadsFolderPath, file);

            return Ok(storePhoto);
        }
    }
}